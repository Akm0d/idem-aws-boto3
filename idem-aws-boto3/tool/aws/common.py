import asyncio
from asyncio import FIRST_EXCEPTION
import aiobotocore


def __init__(hub):
    hub.SESSION = aiobotocore.get_session()

    # Allow up to 24 in-flight API calls.
    hub.IN_FLIGHT_SEMAPHORE = asyncio.Semaphore(value=24, loop=asyncio.get_event_loop())


async def gather_pending_tasks(hub, task_list):
    """
    This function collects completed exec functions, and collects any exceptions recorded during their execution.
    A list of results is returned.

    We really shouldn't get an exception, but if we do, it's appended to results in (False, "exception string")
    format to match the format of failures that might be returned by the exec function itself.

    This way, we only need to return one list of results, which should contain standard exec function result tuples.
    """
    cur_tasks = task_list
    if not len(cur_tasks):
        return []
    results = []

    while True:
        done_list, cur_tasks = await asyncio.wait(cur_tasks, return_when=FIRST_EXCEPTION)
        for done_item in done_list:
            try:
                results.append(done_item.result())
            except Exception as e:
                results.append((False, f"{e.__class__.__name__}: {str(e)}"))
        if not len(cur_tasks):
            break
    return results


async def simple_api_call(hub, ctx, svc, api_call_name, **call_args):
    """
    This function performs a simple API call, to a service `svc`, calling the aiobotcore method `api_call_name`
    and passing `call_args` as keyword arguments. One of the following will be returned:

    1. `( False, "error string" )`
    2. `( True, <full aiobotocore response> )`
    """
    try:
        async with hub.SESSION.create_client(svc) as client:
            api_func = getattr(client, api_call_name)
            async with hub.IN_FLIGHT_SEMAPHORE:
                result = await api_func(**call_args)
            return True, result
    except Exception as e:
        return False, f"{e.__class__.__name__}: {str(e)}"


async def list_paginated(hub, ctx, svc, api_call_name, result_label, **kwargs):
    """
    List available IAM paginated elements. You tell me the API call to call
    in `api_call_name`, and where to look for result elements in the
    aiobotocore result by specifying `result_label`.

    Note that this function is also safe to call for non-paginated results,
    in which case we simply get all results with a single call.

    We will 'unpack' and collect all the results found in the `result_label`
    list for each API call and return these only, rather than return the
    entire API call response(s).

    Example call::

      hub.tools.aws.list_paginated(ctx, "iam", "list_groups", "Groups")

    You will get a list of groups back in a list.

    Note that any extra keyword arguments will get passed to the botocore
    call as keyword arguments.

    botocore supports the ability to specify `MaxItems`. `MaxItems` can be
    specified as a keyword argument to this function, which defaults to 100.

    Note that this function
    *will* always return all results, even with `MaxItems` set to a lower or
    higher value, since it will automatically paginate through results. You
    would set `MaxItems` for performance reasons, to increase the # returned
    per API call, or set it artificially low to test pagination.

    What is returned is a boolean indicating success or failure, and a list of
    entities, or an error string indicating failure reason.

    """
    try:
        async with hub.SESSION.create_client(svc) as client:
            marker = None
            results = []
            call_args = kwargs.copy()
            api_func = getattr(client, api_call_name)
            while True:
                if marker:
                    call_args["Marker"] = marker
                async with hub.IN_FLIGHT_SEMAPHORE:
                    result = await api_func(**call_args)
                results += result[result_label]
                if "IsTruncated" in result and result["IsTruncated"] is True:
                    marker = result["Marker"]
                    continue
                else:
                    break
            return True, results
    except Exception as e:
        return False, f"{e.__class__.__name__}: {str(e)}"
