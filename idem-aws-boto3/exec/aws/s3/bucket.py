import logging

import aiobotocore


BUCKET_METHODS = {
    "get": [
        "acl",
        "analytics_configuration",
        "cors",
        "encryption",
        "inventory_configuration",
        "lifecycle",
        "lifecycle_configuration",
        "location",
        "logging",
        "metrics_configuration",
        "notification",
        "notification_configuration",
        "ownership_controls",
        "policy",
        "policy_status",
        "replication",
        "request_payment",
        "tagging",
        "versioning",
        "website",
    ]
}


def wrap_get_method(hub, str_id):
    """
    Given a `str_id` that gets passed to our wrapper, we are going to return the function name to create in this plugin,
    along with a wrapper that will pass `str_id` to our meta-function.

    The function we are returning will expect arguments of `ctx` and `name` -- context and the name of the bucket.
    But it will internally call `get_subitem(hub, ctx, str_id, name)` to get this value.
    """

    async def wrapper(ctx, name: str):
        return await get_subitem(hub, ctx, str_id, name)

    return f"get_{str_id}", wrapper


async def get_subitem(hub, ctx, str_id, bucket_name: str):
    """
    There are a lot of S3 bucket operations that have the came calling convention. They require a `name` for a bucket,
    and query this particular set of properties, and then return results.

    This is a meta-function which can implement any of these property queries. But it needs a `str_id` which is used
    to identify the underlying property it is querying.

    :param hub: pop hub.
    :param ctx: context.
    :param str_id: string identifier for S3 bucket property in aiobotocore API.
    :param bucket_name: name of bucket
    :return: Tuple containing True and serialized result of API call, or False and error message string on failure.
    """
    try:
        session = aiobotocore.get_session()
        async with session.create_client("s3") as client:
            func = getattr(client, f"get_bucket_{str_id}")
            result = await func(Bucket=bucket_name)
            return True, result
    except Exception as e:
        return False, f"{e.__class__.__name__}: {str(e)}"


def __func_alias__(hub):
    """
    By defining __func_alias__ as a callable function, we can use python code to "build up" mappings between
    functions in this namespace and the functions that they should call, using python code. So we can have
    conditionals. We could even inspect another python module or a third-party object and dynamically create
    mappings as needed.
    """

    # First, we start with a static mapping -- "list" is a python built-in, so we normally wouldn't be able to
    # have a 'list' method in our plugin.
    out = {"list_": "list"}

    # We also want to process the BUCKET_METHODS data structure, and define wrappers based on its contents.
    # For an example of how this works, one of the values is "acl".
    #
    # "acl" represents that there is an aiobotocore method called "get_bucket_acl", which is on the aiobotocore
    # `Client` object that we can call.
    #
    # Instead of writing 20+ functions named "get_acl", "get_foo", "get_bar", I want to simply write one piece
    # of code that can do all the things, and then create function aliases to functions that handle all these
    # calls -- like "routes".
    #
    # For each str_id, such as "acl", I am going to call `wrap_get_method(hub, str_id)`. This is going to return
    # the `name` and the `func` of the mapping.
    #
    # I am going to dynamically build up the __func_alias__ dict, and then just return it as the result of the
    # function. Then pop will simply use this dict I return as the __func_alias__ map, as if I had magically
    # done all this statically.

    for str_id in BUCKET_METHODS["get"]:
        name, func = wrap_get_method(hub, str_id)
        out[name] = func
    return out


async def list_(hub, ctx):
    session = aiobotocore.get_session()
    async with session.create_client("s3") as client:
        result = await client.list_buckets()
        return True, result["Buckets"]


async def delete(hub, ctx, name: str, **kwargs):
    try:
        session = aiobotocore.get_session()
        async with session.create_client("s3") as client:
            result = await client.delete_bucket(Bucket=name)
            return True, result
    except Exception as e:
        return False, f"{e.__class__.__name__}: {str(e)}"


async def create(hub, ctx, name: str, **kwargs):
    try:
        session = aiobotocore.get_session()
        async with session.create_client("s3") as client:
            if client.meta.region_name is not None and "LocationConstraint" not in kwargs:
                # For region-specific endpoints, leaving the region constraint empty results in API error:
                kwargs["LocationConstraint"] = client.meta.region_name
            result = await client.create_bucket(Bucket=name, CreateBucketConfiguration=kwargs)
            return True, result
    except Exception as e:
        return False, f"{e.__class__.__name__}: {str(e)}"


async def get_acl(hub, ctx, name: str):
    return await get_subitem(hub, ctx, "acl", name)
