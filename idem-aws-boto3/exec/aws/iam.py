# See this file for details of structure of return values, etc.
#
# https://botocore.amazonaws.com/v1/documentation/api/latest/reference/services/iam.html
import asyncio


#####################################################################################################
# Basic listing of core IAM objects
#####################################################################################################
from asyncio import Task


async def list_groups(hub, ctx, path_prefix="/"):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_groups", "Groups", PathPrefix=path_prefix)


async def list_users(hub, ctx, path_prefix="/"):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_users", "Users", PathPrefix=path_prefix)


async def list_roles(hub, ctx, path_prefix="/"):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_roles", "Roles", PathPrefix=path_prefix)


async def list_instance_profiles(hub, ctx, path_prefix="/"):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_instance_profiles", "InstanceProfiles", PathPrefix=path_prefix)


async def list_server_certificates(hub, ctx, path_prefix="/"):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_server_certificates", "ServerCertificateMetadataList", PathPrefix=path_prefix,)


async def list_saml_providers(hub, ctx):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_saml_providers", "SAMLProviderList")


async def list_open_id_connect_providers(hub, ctx):
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_open_id_connect_providers", "OpenIDConnectProviderList")


#####################################################################################################
# List/Get User Attributes
#####################################################################################################


async def list_access_keys(hub, ctx, username=None):
    call_args = {"UserName": username}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_access_keys", "AccessKeyMetadata", **call_args)


async def list_account_aliases(hub, ctx, username=None):
    call_args = {"UserName": username}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_account_aliases", "AccountAliases", **call_args)


async def list_groups_for_user(hub, ctx, username=None):
    call_args = {"UserName": username}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_groups_for_user", "Groups", **call_args)


async def list_ssh_public_keys(hub, ctx, username=None):
    call_args = {"UserName": username}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_ssh_public_keys", "SSHPublicKeys", **call_args)


async def get_ssh_public_key(hub, ctx, username=None, key_id=None, encoding="ssh"):
    call_args = {"UserName": username, "SSHPublicKeyId": key_id, "Encoding": encoding.upper()}
    return await hub.tool.aws.common.simple_api_call(ctx, "iam", "get_ssh_public_key", **call_args)


#####################################################################################################
# Add/Modify User Attributes
#####################################################################################################


async def upload_ssh_public_key(hub, ctx, username=None, key=None):
    call_args = {"UserName": username, "SSHPublicKeyBody": key}
    return await hub.tool.aws.common.simple_api_call(ctx, "iam", "upload_ssh_public_key", **call_args)


async def update_ssh_public_key(hub, ctx, username=None, key_id=None, status="active"):
    call_args = {"UserName": username, "SSHPublicKeyId": key_id, "Status": status.capitalize()}
    return await hub.tool.aws.common.simple_api_call(ctx, "iam", "update_ssh_public_key", **call_args)


#####################################################################################################
# List Group Attributes
#####################################################################################################


async def list_attached_group_policies(hub, ctx, groupname=None):
    call_args = {"GroupName": groupname}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_attached_group_policies", "AttachedPolicies", **call_args)


#####################################################################################################
# List Role Attributes
#####################################################################################################


async def list_attached_role_policies(hub, ctx, rolename=None):
    call_args = {"RoleName": rolename}
    return await hub.tool.aws.common.list_paginated(ctx, "iam", "list_attached_role_policies", "AttachedPolicies", **call_args)


#####################################################################################################
# Associate Objects
#####################################################################################################


async def add_user_to_group(hub, ctx, username=None, groupname=None):
    call_args = {"UserName": username, "GroupName": groupname}
    return await hub.tool.aws.common.simple_api_call(ctx, "iam", "add_user_to_group", **call_args)


#####################################################################################################
# Create Objects
#####################################################################################################


async def create_group(hub, ctx, groupname=None, path="/"):
    call_args = {"GroupName": groupname, "Path": path}
    return await hub.tool.aws.common.simple_api_call(ctx, "iam", "create_group", **call_args)


#####################################################################################################
# Higher-level functions (for states)
#####################################################################################################


async def ssh_key_present(hub, ctx, username=None, key=None):
    """
    Ensure the specified ssh key exists for this username, and is active. This exec function is
    designed to support the implementation of a ssh_key.present state.

    `username` is self explanatory. `key` should be the literal `ssh-rsa asdf;laskdf` keydata
    in OpenSSH format.

    This function works as follows. It will list all the ssh keys for a user, get key IDs, then
    query extended information for each of these IDs, so we have the literal SSH key data client-
    side. Then we will make sure that `key` exists and is active.

    If it is, great. If not active, we use the specific API call to make it active. If it doesn't
    exist, we will upload the key.

    Returns a tuple -- a boolean indicating success or failure, and a string indicating what
    action was taken.

    IMPORTANT NOTE: This function is not fast. But it is correct and complete, and leverages
    several exec functions which is good for testing. A future version should do client-side
    caching for SSH key data so it doesn't have to keep re-querying AWS for this with each call.
    """

    # This removes the trailing email/identifier string from the end of the key if present. AWS
    # does not store this, so for string comparisons, we must remove it from ours too:

    key = key.strip()
    key_split = key.split()
    key = " ".join(key_split[:2])

    # List all ssh keys and get their AWS 'ids':

    success, result = await list_ssh_public_keys(hub, ctx, username=username)
    if success is not True:
        return False, result
    ssh_key_ids = list(map(lambda x: x["SSHPublicKeyId"], result))
    key_reverse_mappings = {}

    # Grab extended data for all SSH keys, all at once:

    grabber_tasks = []

    for key_id in ssh_key_ids:
        grabber_tasks.append(Task(get_ssh_public_key(hub, ctx, username=username, key_id=key_id)))
    results = await hub.tool.aws.common.gather_pending_tasks(grabber_tasks)

    for success, result in results:
        if not success:
            return False, result
        key_obj = result["SSHPublicKey"]
        key_reverse_mappings[key_obj["SSHPublicKeyBody"]] = key_obj

    # Find our SSH key:

    if key in key_reverse_mappings:
        key_id = key_reverse_mappings[key]["SSHPublicKeyId"]
        # key exists -- but is it active?
        if key_reverse_mappings[key]["Status"] == "Active":
            return True, f"Key ID {key_id} already exists in correct state."
        else:
            # Inactive -- make active.
            success, result = await update_ssh_public_key(hub, ctx, username=username, key_id=key_id, status="active")
            if success:
                return success, f"Key ID {key_id} set to active status."
            else:
                return False, result
    else:
        # key does not exist
        success, result = await upload_ssh_public_key(hub, ctx, username=username, key=key)
        if success:
            return success, "Key uploaded."
        else:
            return False, result
