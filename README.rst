==============
idem-aws-boto3
==============

``idem-aws-boto3`` allows you to define AWS infrastructure in a YAML file, and maintain this infrastructure by
non-destructively applying these states to your AWS environment. In the following example, the
``my-aws-yaml.sls`` file contains YAML infrastructure definitions::

  $ idem state my-aws-yaml.sls

Once the command completes, your AWS environment will be updated to reflect the state defined in the YAML.
It uses familiar YAML-based `salt`_ state paradigms for interacting with AWS, and a future version of `salt`_ will
be able to use ``idem-aws-boto3`` to manage AWS.

You can also use the ``idem exec`` command as a powerful AWS command-line to query AWS resources and apply imperative
commands. For example::

  $ idem exec aws.s3.bucket.create foobaroni
  $ idem exec aws.s3.bucket.list

output::

  - True
    |_
      |_
        ----------
        name:
            foobaroni
        creation_date:
            2020-11-03 19:25:17+00:00
        obj:
            s3.Bucket(name='foobaroni')
      |_
        ----------
        name:
            super-bucket-2020-11-03
        creation_date:
            2020-11-03 18:18:29+00:00
        obj:
            s3.Bucket(name='super-bucket-2020-11-03')
      |_

Design Goals
============

AWS has a lot of services, and will continually be adding new services. We want to be able to manage these services
`idempotently`_ using `salt`_. We want to make it extremely easy to add new AWS services to ``idem-aws-boto3``.

There are a lot of people out there who are experienced with AWS, have some python knowledge, but are not full-time
software developers. They're busy and are looking for something to make their lives easier, and care a lot about good
cloud infrastructure management. They might have a title of "DevOps Engineer" at their day job.

We consider these folks "our people". We want this code to be useful to them. Moreover, it should be easy for them to
extend this code, without being experts in `POP`_, nor experts in Python. An afternoon with our documentation and a
bit of boto3 hackery should be all that's needed for a typical DevOps Engineer to extend ``idem-aws-boto3`` to support a
new AWS service.

To achieve these ends, this project has the following design goals:

1. Keep code as *clean and simple as possible*. Keep functions under 30 lines of code each. Keep internal
   code for AWS simple and self-contained. This allows contributors to have an easy
   time extending ``idem-aws-boto3`` to support new AWS services by adding a new plugin.
   Plugin code should appear as similar to stand-alone (aio)boto3 code as possible.

2. Default end-result *behavior* of states and execution modules should not deviate meaningfully from the
   stand-alone code written purely in ``aiobotocore``. This helps people who are transitioning from ``awscli``
   or ``aiobotocore`` to have an intuitive experience.

3. *Implementation* of stateful AWS services should not require any additional AWS functionality than
   that used by ``aiobotocore``. Any additional functionality required, such as tracking of resources, should be
   implemented client-side. This is an area of future R&D work.

4. *New functionality and capabilities* beyond boto3 can be added but should be opt-in, and default
   ``aiobotocore`` code paths should be used by default. For example, authentication leverages ``boto3``
   credentials code by default, with the ability to opt-in to POP's `acct`_ and other more advanced systems as
   desired. This ensures that the standard boto3 behavior and configuration is always available as a reasonable
   default and fallback, improving consistency and testability since we can then easily A/B test the default
   boto3 code paths with any of our own code.

5. *Data structures and return values* *may* be normalized somewhat to provide a more consistent AWS
   experience across services. This is a known problem with AWS in general.

6. Try to support initial handful of AWS services so that the project can be used immediately for useful tasks.

7. Have good and up-to-date documentation for beginners as well as experts.

Architecture
============

It is built on the following foundational components:

`boto3`_
  Boto is used to interact with AWS under the hood. Technically, we don't actually use ``boto3`` directly but
  instead use `aiobotocore`_, which is an asyncio wrapper for botocore. Botocore is a lower-level API upon
  which `boto3`_ is based. This gives us objects in a serialized format, which is ideal for salt.

`idem`_
  Idem is a next-generation state and execution module framework, for defining function as well as `idempotent`_ states.
  A future version of `salt`_ will be able to utilize ``idem`` states and execution modules directly. You can also
  directly leverage states and execution modules via the command-line using the ``idem`` command, similar to using
  ``salt-cmd``. If you are new to either `salt`_ or `idem`_, what this means is that you can define desired AWS
  infrastructure in YAML, and then apply these states to AWS to configure your resources as needed. If you have used
  ``salt`` before, think of ``idem`` as the next-generation way to write states and execution modules (imperative
  functions.)

`acct`_
  Acct is a framework for accessing and securely storing account credentials for a variety of different environments,
  including AWS. It allows you to put your credentials in YAML and then store them on disk using `Fernet`_ encryption.
  By default, ``idem-aws-boto3`` will use the underlying credentials that boto3 uses, but ``acct`` support is available
  as an option for enhanced security and functionality.

`POP`_
  `POP`_ is the underlying meta-framework tying everything together. Both ``idem`` and ``acct`` leverage the POP framework.
  POP stands for "Plugin-Oriented Programming", and the POP framework implements this paradigm. This paradigm allows
  for the creation of "pluggable" commands that can be easily extended.

Salt Cloud Ecosystem
====================

We already have a project called ``idem-aws``, which is supposed to do much of the same thing as ``idem-aws-boto3``.
So it's fair question to ask how ``idem-aws-boto3`` fits into the POP cloud ecosystem.

``idem-aws`` is a more sophisticated architecture designed to support multiple backend implementations of AWS APIs,
whereas ``idem-aws-boto3`` is focused purely on (aio)boto3.

The projects at this time are separate. We are creating the new project to test out a slightly different design and
packaging philosophy for a cloud plugin. ``idem-aws-boto3`` is the leaner and more streamlined architecture. It should
be simpler to develop, maintain, test and extend and have a smaller and simpler codebase. It is more
contributor-friendly.

In the future, it is possible that ``idem-aws`` and ``idem-aws-boto3`` will connect in interesting ways. We may use
``idem-aws`` to deliver a higher-level AWS API. This gives us a nice separation between our higher-level code that will
leverage more advanced (and complex) `POP`_ and infra management concepts and the code that simply wraps ``aiobotocore``
and is easily extensible by contributors -- the best of both worlds.

I hope I have explained the goals of this effort clearly enough so you can see the vision and how ``idem-aws-boto3``
fits into the salt cloud ecosystem.

.. _Fernet: https://medium.com/coinmonks/if-youre-struggling-picking-a-crypto-suite-fernet-may-be-the-answer-95196c0fec4b
.. _idem: https://pypi.org/project/idem/
.. _boto3: https://pypi.org/project/boto3/
.. _aiobotocore: https://pypi.org/project/botocore/
.. _salt: https://pypi.org/project/salt/
.. _acct: https://gitlab.com/saltstack/pop/acct
.. _idempotent: https://en.wikipedia.org/wiki/Idempotence
.. _idempotently: https://en.wikipedia.org/wiki/Idempotence
.. _POP: https://pypi.org/project/pop/
